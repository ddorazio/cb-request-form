var validateAdminMode = function () {
    $.ajax({
        type: "GET",
        url: config.BaseUrl + "api/FormRequest/GetMaintenanceMode",
        //data: JSON.stringify(dataObject),
        contentType: 'application/json; charset=utf-8',
        datatype: "JSON",
        success: function (response) {

            if (response.data === false) {

                $('#initial-loading').addClass('d-none');
                return;
            }

            $('#maintenanceLoadingSpinner').hide();
            $('#maintenanceLoading').hide();
            $('#maintenanceMessage').show();
        },
        error: function (error) {
            setTimeout(() => {
                $('#loading').addClass('d-none');
                $('#error').removeClass('d-none');
            }, 2000);

            console.log(error);
        }
    });
};


$('#submitRequest').click(function () {

    // Disable button to avoid delays and duplicate requests
    //$("#submitRequest").attr("disabled", true);

    // Check if the amount is selected
    if ($('#amount').val() === "") {
        return;
    }

    // Check if the terms and conditions was checked
    if ($('#terms')[0].checked === false) {
        return;
    }

    // Check if the bankruptcy condition is c
    if ($('#bankruptcy')[0].checked === false && $('#noBankruptcy')[0].checked === false) {
        return;
    }

    // Add Splash screen right away on Submit
    $('#loading').removeClass('d-none');

    getIPAddress().then((data) => {

        var ip = data.ip;

        submitForm(ip).then((data) => {
            console.log(data)

        })
            .catch((error) => {
                console.log(error)
            })

    })
        .catch((error) => {
            console.log(error)
        })
    //});
});

var submitForm = function (ip) {

    return new Promise((resolve, reject) => {

        // Get all data from form
        //var amount = $('#amount option:selected').text();
        var amount = $('#amount').val();
        var firstName = $('#firstName').val();
        var lastName = $('#lastName').val();
        //var birthday = $('#birthday').val();
        var birthday = $('#birthdayYear').val() + "-" + $('#birthdayMonth').val() + "-" + $('#birthdayDay').val();

        var languageCode = $('#language').val();
        var language = $('#language option:selected').text();

        var phoneNumber = $('#phone').unmask().val();
        var cellPhoneNumber = $('#celphone').unmask().val();
        var primaryEmail = $('#email1').val();
        var secondaryEmail = $('#email2').val() !== "" ? $('#email2').val() : null;

        //var residenceStartDate = $('#dateMoveIn').val();
        var residenceStartDate = $('#dateMoveInYear').val() + "-" + $('#dateMoveInMonth').val() + "-" + $('#dateMoveInDay').val();

        var civicNumber = $('#address').val();
        var streetName = $('#street').val();
        var aptNo = $('#appartment').val();
        var city = $('#city').val();
        var province = $('#province').val();
        var postalCode = $('#postalCode').val();

        var addressStatus = null;
        var isRenter = null;
        var grossAnnualSalary = null;
        var monthlyPaymenet = null;
        var monthlyElectricalPayment = null;
        var monthlyCarPayment = null;
        var montlyAppliancePayment = null;

        if (province === 'QC') {
            addressStatus = $("input[name='addressStatus']:checked").val();
            isRenter = addressStatus === 'tenant' ? true : false;
            grossAnnualSalary = $('#grossSalary').unmask().val();
            monthlyPaymenet = $('#addressCost').unmask().val();
            monthlyElectricalPayment = $('#costElectricity').unmask().val();
            monthlyCarPayment = $('#carLoan').unmask().val();
            montlyAppliancePayment = $('#otherLoan').unmask().val();
        }

        var ref1FirstName = $('#ref1_firstName').val();
        var ref1LastName = $('#ref1_lastName').val();
        var ref1Telephone = $('#ref1_phone').unmask().val();
        var ref1Relationship = $('#ref1_kinship').val();
        //var ref1Email = $('#ref1_email').val();

        var ref2FirstName = $('#ref2_firstName').val();
        var ref2LastName = $('#ref2_lastName').val();
        var ref2Telephone = $('#ref2_phone').unmask().val();
        var ref2Relationship = $('#ref2_kinship').val();
        //var ref2Email = $('#ref2_email').val();

        var incomeTypeCode = $("input[name='incomeSourceOption']:checked").val();

        var payFrequencyCode = null;
        //var payFrequency = null;
        var nextDepositDate = null;

        if (incomeTypeCode === 'EMPLOYEE') {
            payFrequencyCode = $('#payroll').val();
            //payFrequency = $('#payroll option:selected').text();
        }

        var occupation = $('#occupation').val();
        var employerName = $('#companyName').val();
        //var hiringDate = $('#hireDate').val();

        var supervisorName = $('#supervisorName').val();
        var employerPhoneNumber = $('#job_phone').unmask().val();
        var employerExtension = $('#job_phone_ext').val();
        //var nextPayDate = $('#nextPayDate').val();

        if (incomeTypeCode === 'EMPLOYEE') {
            var hiringDate = $('#hireDateYear').val() + "-" + $('#hireDateMonth').val() + "-" + $('#hireDateDay').val();
            var nextPayDate = $('#nextPayDateYear').val() + "-" + $('#nextPayDateMonth').val() + "-" + $('#nextPayDateDay').val();
        }

        var employmentInsuranceStartDate = ($('#EA_StartDateYear').val() === "" || $('#EA_StartDateMonth').val() === "" || $('#EA_StartDateDay').val() === "")
            ? null
            : $('#EA_StartDateYear').val() + "-" + $('#EA_StartDateMonth').val() + "-" + $('#EA_StartDateDay').val();

        var employmentInsuranceNextPayDate = ($('#EA_NextPayYear').val() === "" || $('#EA_NextPayMonth').val() === "" || $('#EA_NextPayDay').val() === "")
            ? null
            : $('#EA_NextPayYear').val() + "-" + $('#EA_NextPayMonth').val() + "-" + $('#EA_NextPayDay').val();

        if (incomeTypeCode === 'SELF_EMPLOYED') {
            payFrequencyCode = $('#selfEmployed-depositFrequency').val();
            //nextDepositDate = $('#selfEmployed-depositDate').val();
            nextDepositDate = $('#selfEmployed-depositDateYear').val() + "-" + $('#selfEmployed-depositDateMonth').val() + "-" + $('#selfEmployed-depositDateDay').val();
        }

        var selfEmployedDirectDeposit = $("input[name='selfEmployed-directDeposit']:checked").val();
        var directDeposit = selfEmployedDirectDeposit === 'yes' ? true : false;

        var selfEmployedStartDate = ($('#selfEmployed-startDateYear').val() === "" || $('#selfEmployed-startDateMonth').val() === "" || $('#selfEmployed-startDateDay').val() === "")
            ? null
            : $('#selfEmployed-startDateYear').val() + "-" + $('#selfEmployed-startDateMonth').val() + "-" + $('#selfEmployed-startDateDay').val();

        var selfEmployedWorkTelephone = $('#selfEmployed-phone').unmask().val() === "" ? null : $('#selfEmployed-phone').unmask().val();

        if (incomeTypeCode === 'DISABILITY') {
            //nextDepositDate = $('#disabilityBenefits_NextPay').val();
            nextDepositDate = $('#disabilityBenefits_NextPayYear').val() + "-" + $('#disabilityBenefits_NextPayMonth').val() + "-" + $('#disabilityBenefits_NextPayDay').val();
        }

        if (incomeTypeCode === 'PARENTAL_INSURANCE') {
            //nextDepositDate = $('#parentalInsurancePlan_NextPay').val();
            nextDepositDate = $('#parentalInsurancePlan_NextPayYear').val() + "-" + $('#parentalInsurancePlan_NextPayMonth').val() + "-" + $('#parentalInsurancePlan_NextPayDay').val();
        }

        var loanType = $("input[name='loanType']:checked").val();
        var loanWithoutDocuments = loanType === 'IBV' ? true : false;

        var declaringBankrupcy = $('#yes')[0].checked;

        var dataObject = {
            Amount: amount,
            FirstName: firstName,
            LastName: lastName,
            Birthday: birthday,
            Language: language,
            LanguageCode: languageCode,
            //PhoneNumber: phoneNumber,
            CellPhoneNumber: cellPhoneNumber,
            PrimaryEmail: primaryEmail,
            //SecondaryEmail: secondaryEmail,

            CivicNumber: civicNumber,
            StreetName: streetName,
            Province: province,
            City: city,
            AptNo: aptNo,
            ResidenceStartDate: residenceStartDate,
            PostalCode: postalCode,

            Ref1FirstName: ref1FirstName,
            Ref1LastName: ref1LastName,
            Ref1Telephone: ref1Telephone,
            Ref1Relationship: ref1Relationship,
            //Ref1Email: ref1Email,

            Ref2FirstName: ref2FirstName,
            Ref2LastName: ref2LastName,
            Ref2Telephone: ref2Telephone,
            Ref2Relationship: ref2Relationship,
            //Ref2Email: ref2Email,

            IsRenter: isRenter,
            MonthlyPaymenet: monthlyPaymenet,
            MonthlyElectricalPayment: monthlyElectricalPayment,
            MonthlyCarPayment: monthlyCarPayment,
            MontlyAppliancePayment: montlyAppliancePayment,
            GrossAnnualSalary: grossAnnualSalary,

            PayFrequencyCode: payFrequencyCode,
            //PayFrequency: payFrequency,
            IncomeTypeCode: incomeTypeCode,
            EmployerName: employerName,
            EmployerPhoneNumber: employerPhoneNumber,
            EmployerExtension: employerExtension,
            SupervisorName: supervisorName,
            Occupation: occupation,
            NextPayDate: nextPayDate,
            HiringDate: hiringDate,

            EmploymentInsuranceStartDate: employmentInsuranceStartDate,
            EmploymentInsuranceNextPayDate: employmentInsuranceNextPayDate,
            DirectDeposit: directDeposit,
            SelfEmployedStartDate: selfEmployedStartDate,
            SelfEmployedWorkTelephone: selfEmployedWorkTelephone,
            NextDepositDate: nextDepositDate,
            LoanWithoutDocuments: loanWithoutDocuments,

            IpAddress: ip,
            ReferenceNo: referenceNo,

            GoogleId: googleId,
            GoogleEmail: googleEmail,
            GooglePhoto: googlePhoto,
            GoogleFirstName: googleFirstName,
            GoogleLastName: googleLastName,

            DeclaringBankrupcy: declaringBankrupcy,

            FacebookId: facebookId,
            FacebookPhoto: facebookPhoto,
            FacebookEmail: facebookEmail,
            FacebookFirstName: facebookFirstName,
            FacebookLastName: facebookLastName
        };

        $.ajax({
            type: "POST",
            url: config.BaseUrl + "api/FormRequest/NewLoanRequest",
            data: JSON.stringify(dataObject),
            contentType: 'application/json; charset=utf-8',
            datatype: "JSON",
            success: function (response) {

                resolve(response);

                // Auto decline
                if (response.code === "DECLINED") {

                    setTimeout(() => {
                        $('#loading').addClass('d-none');
                        $('#decline').removeClass('d-none');
                    }, 5000);

                    // Show message to user that the request was decline and then exit loop
                    return;
                }

                // Success
                if (response.code === "SUCCESS") {

                    // Show message to user that the request was successfull and redirect to IBV if requested
                    if (loanWithoutDocuments === true) {

                        setTimeout(() => {
                            $('#loading').addClass('d-none');
                            $('#success_ibv').removeClass('d-none');
                        }, 5000);

                        setTimeout(() => { window.location.replace(response.data.url); }, 5000);

                        // Show message to user that the request was successfull
                    } else {

                        setTimeout(() => {
                            $('#loading').addClass('d-none');
                            $('#success').removeClass('d-none');
                        }, 5000);
                    }
                }
            },
            error: function (error) {
                setTimeout(() => {
                    $('#loading').addClass('d-none');
                    $('#error').removeClass('d-none');
                }, 2000);

                console.log(error);
                reject(error);
            }
        });

    });
}

var submitNonCompleted = function () {

    // Get all data from form
    var firstName = $('#firstName').val();
    var lastName = $('#lastName').val();
    //var birthday = $('#birthday').val();
    var birthday = $('#birthdayYear').val() + "-" + $('#birthdayMonth').val() + "-" + $('#birthdayDay').val();

    var languageCode = $('#language').val();
    var language = $('#language option:selected').text();

    //var phoneNumber = $('#phone').unmask().val();
    var cellPhoneNumber = $('#celphone').unmask().val();
    var primaryEmail = $('#email1').val();
    //var secondaryEmail = $('#email2').val() !== "" ? $('#email2').val() : null;

    var dataObject = {
        FirstName: firstName,
        LastName: lastName,
        Birthday: birthday,
        Language: language,
        LanguageCode: languageCode,
        //PhoneNumber: phoneNumber,
        CellPhoneNumber: cellPhoneNumber,
        PrimaryEmail: primaryEmail,
        //SecondaryEmail: secondaryEmail,

        GoogleId: googleId,
        GoogleEmail: googleEmail,
        GooglePhoto: googlePhoto,
        GoogleFirstName: googleFirstName,
        GoogleLastName: googleLastName,

        FacebookId: facebookId,
        FacebookPhoto: facebookPhoto,
        FacebookEmail: facebookEmail,
        FacebookFirstName: facebookFirstName,
        FacebookLastName: facebookLastName
    };

    $.ajax({
        type: "POST",
        url: config.BaseUrl + "api/FormRequest/NonCompletedRequest",
        data: JSON.stringify(dataObject),
        contentType: 'application/json; charset=utf-8',
        datatype: "JSON",
        success: function (response) {

            // Set reference no
            referenceNo = response.data;
        },
        error: function (error) {
            console.log(error);
        }
    });
}

var getIPAddress = function () {

    return new Promise((resolve, reject) => {

        $.ajax({
            type: "GET",
            url: "https://api.ipify.org?format=json",
            contentType: 'application/json; charset=utf-8',
            datatype: "JSON",
            success: function (data) {
                resolve(data);
            },
            error: function (error) {

                console.log(error);
                reject(error);
            }
        });

    });

};