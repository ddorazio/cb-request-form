// ------------step-wizard-------------
$(document).ready(function () {


    //$('#initial-loading').removeClass('d-none');
    //setTimeout(() => { validateAdminMode(); }, 3000);

    //auth2 = gapi.auth2.getAuthInstance();
    // Check if User is already logged in with Google
    //validateGoogleLoggedIn();
    history.replaceState(null, null, document.location.origin);

    activateMasks();

    $.dobPicker({
        daySelector: '#birthdayDay',
        monthSelector: '#birthdayMonth',
        yearSelector: '#birthdayYear',
        dayDefault: '',
        monthDefault: '',
        yearDefault: '',
        minimumAge: 0
        //maximumAge: 100
    });

    $.dobPicker({
        daySelector: '#dateMoveInDay',
        monthSelector: '#dateMoveInMonth',
        yearSelector: '#dateMoveInYear',
        dayDefault: '',
        monthDefault: '',
        yearDefault: '',
        minimumAge: 0
        //maximumAge: 100
    });

    $.dobPicker({
        daySelector: '#hireDateDay',
        monthSelector: '#hireDateMonth',
        yearSelector: '#hireDateYear',
        dayDefault: '',
        monthDefault: '',
        yearDefault: '',
        minimumAge: 0
        //maximumAge: 100
    });

    $.dobPicker({
        daySelector: '#nextPayDateDay',
        monthSelector: '#nextPayDateMonth',
        yearSelector: '#nextPayDateYear',
        dayDefault: '',
        monthDefault: '',
        yearDefault: '',
        minimumAge: 0
        //maximumAge: 100
    });

    $.dobPicker({
        daySelector: '#EA_StartDateDay',
        monthSelector: '#EA_StartDateMonth',
        yearSelector: '#EA_StartDateYear',
        dayDefault: '',
        monthDefault: '',
        yearDefault: '',
        minimumAge: 0
        //maximumAge: 100
    });

    $.dobPicker({
        daySelector: '#EA_NextPayDay',
        monthSelector: '#EA_NextPayMonth',
        yearSelector: '#EA_NextPayYear',
        dayDefault: '',
        monthDefault: '',
        yearDefault: '',
        minimumAge: 0
        //maximumAge: 100
    });

    $.dobPicker({
        daySelector: '#selfEmployed-startDateDay',
        monthSelector: '#selfEmployed-startDateMonth',
        yearSelector: '#selfEmployed-startDateYear',
        dayDefault: '',
        monthDefault: '',
        yearDefault: '',
        minimumAge: 0
        //maximumAge: 100
    });

    $.dobPicker({
        daySelector: '#selfEmployed-depositDateDay',
        monthSelector: '#selfEmployed-depositDateMonth',
        yearSelector: '#selfEmployed-depositDateYear',
        dayDefault: '',
        monthDefault: '',
        yearDefault: '',
        minimumAge: 0
        //maximumAge: 100
    });

    $.dobPicker({
        daySelector: '#disabilityBenefits_NextPayDay',
        monthSelector: '#disabilityBenefits_NextPayMonth',
        yearSelector: '#disabilityBenefits_NextPayYear',
        dayDefault: '',
        monthDefault: '',
        yearDefault: '',
        minimumAge: 0
        //maximumAge: 100
    });

    $.dobPicker({
        daySelector: '#parentalInsurancePlan_NextPayDay',
        monthSelector: '#parentalInsurancePlan_NextPayMonth',
        yearSelector: '#parentalInsurancePlan_NextPayYear',
        dayDefault: '',
        monthDefault: '',
        yearDefault: '',
        minimumAge: 0
        //maximumAge: 100
    });

    // Initialize datepickers
    // $('.date').datepicker({
    //     format: "mm-dd-yyyy",
    //     language: "fr",
    //     autoclose: true,
    //     clearBtn: true
    // });
    /*
        // Vérifier l'url lors de refresh de page pour ne pas recommencer au début
        if (window.location.hash.indexOf('connexion') == 1) {
            $('#connexionPage').removeClass("d-none");
            ($('#openingPage').is('.d-none')) ? '' : $("#openingPage").addClass("d-none");
            ($('#step-container').is('.d-none')) ? '' : $("#step-container").addClass("d-none");
            ($('.sidebar .wizard').is('.d-none')) ? '' : $(".sidebar .wizard").addClass("d-none");
        }
        else if (window.location.hash.indexOf('step') == 1) {
            $('#openingPage').addClass("d-none");
            $('#connexionPage').addClass("d-none");
            $("#step-container").removeClass("d-none");
            $(".sidebar .wizard").removeClass("d-none");
            $(".loanAmount").removeClass("d-none");
            const hash = location.hash.split('#')[1];
            $('.nav-tabs a[href="#' + hash + '"]').click();
     
        }
        else {
            $('#openingPage').removeClass("d-none");
            ($('#connexionPage').is('.d-none')) ? '' : $("#connexionPage").addClass("d-none");
            ($('#step-container').is('.d-none')) ? '' : $("#step-container").addClass("d-none");
            ($('.sidebar .wizard').is('.d-none')) ? '' : $(".sidebar .wizard").addClass("d-none");
        }
    */


    //Wizard
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target);
        if (target.parent().hasClass('disabled')) {
            return false;
        }

        let newUrl;
        var url = document.location.href;
        const hash = $(this).attr("href");
        newUrl = url.split("#")[0] + hash;
        history.replaceState(null, null, newUrl);
    });

    $(".next-step").click(function (e) {
        //Fetch form to apply custom Bootstrap validation
        var form = $("#myForm");
        var stepIsValid = 0;
        var hash = window.location.hash.substr(1);

        // If Province is not QC then hide the Obligations section
        var province = $('#province').val();

        if (province !== 'QC' && hash == 'step2') {
            $(".wizard .nav-tabs li:nth-child(3)").addClass("d-none");
            $("#step3").addClass("d-none");
        }

        if (hash == 'start' || hash == 'step1' || hash == 'connexion' || hash == '') {
            stepIsValid = validateUI("#step1");
        }

        if (hash == 'step2') {
            stepIsValid = validateUI("#step2");
        }

        if (hash == 'step3') {
            stepIsValid = validateUI("#step3");
        }

        if (hash == 'step4') {
            stepIsValid = validateUI("#step4");
        }

        if (hash == 'step5') {
            stepIsValid = validateUI("#" + $("input[name='incomeSourceOption']:checked").val());
        }

        if (form[0].checkValidity() === false && stepIsValid > 0) {
            e.preventDefault();
            e.stopPropagation();
        }
        else {

            if (hash == 'start' || hash == 'step1' || hash == 'connexion') {
                submitNonCompleted();
            }

            //console.log('here');
            var active = $('.wizard .nav-tabs li.active');

            if (province !== 'QC' && hash == 'step2') {
                active.nextAll().eq(1).removeClass('disabled');
                nextTab(active, true);
            } else {
                active.next().removeClass('disabled');
                nextTab(active, false);
            }
        }
        form.addClass('was-validated');
        activateMasks();
    });

    $(".prev-step").click(function (e) {
        e.preventDefault();
        var active = $('.wizard .nav-tabs li.active');
        prevTab(active);
    });
});

//
function activateMasks() {
    // Mask Step 1
    $('#phone').mask('(000) 000-0000');
    $('#celphone').mask('(000) 000-0000');
    $('#dateOfBirth').mask('00-00-0000');

    // Mask Step 2
    $('#dateMoveIn').mask('00-00-0000');
    $('#postalCode').mask('Z0Z 0Z0', { translation: { 'Z': { pattern: /[A-Za-z]/, optional: false } } });

    // Income declaration
    $('#addressCost').mask("####0.00");
    $('#costElectricity').mask("####0.00");
    $('#carLoan').mask("####0.00");
    $('#otherLoan').mask("####0.00");

    // References
    $('#ref1_phone').mask('(000) 000-0000');
    $('#ref2_phone').mask('(000) 000-0000');

    // Revenu source
    $('#hireDate').mask('00-00-0000');
    $('#job_phone').mask('(000) 000-0000');
    $('#job_phone_ext').mask('00000');
    $('#nextPayDate').mask('00-00-0000');

    $('#EA_StartDate').mask('00-00-0000');
    $('#EA_NextPay').mask('00-00-0000');

    $('#selfEmployed-startDate').mask('00-00-0000');
    $('#selfEmployed-phone').mask('(000) 0000-0000');
    $('#selfEmployed-depositDate').mask('00-00-0000');

    $('#disabilityBenefits_NextPay').mask('00-00-0000');
    $('#parentalInsurancePlan_NextPay').mask('00-00-0000');

    // Money fields
    $('.money').mask('000,000,000,000,000', { reverse: true });
}

// Facebook Sign-In
function checkFBStatus() {

    // window.fbAsyncInit = function () {
    //     FB.init({
    //         appId: '214559176651348',
    //         cookie: true,
    //         xfbml: true,
    //         version: 'v8.0'
    //     });

    //     FB.AppEvents.logPageView();
    // };

    FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            FB.logout(function (response) {
                facebookLogin()
            });
            return;
        }
        facebookLogin();
    });
}

function facebookLogin() {
    //$('#social-loading').removeClass('d-none');
    FB.login(function (response) {
        console.log("Login");
        if (response.status === 'connected') {
            facebookLoggedIn = true;
            facebookGetUserInfo();
        } else {

        }
    }, { scope: 'public_profile,email' });
}

function facebookLogout() {
    FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            FB.logout(function (response) {
            });
            facebookLoggedIn = false;
            return;
        }
    });
}

function facebookGetUserInfo() {
    FB.api('/me', { fields: 'last_name, first_name, email' }, function (response) {
        $('#social-loading').addClass('d-none');

        facebookId = response.id;
        facebookPhoto = response.profile_pic;
        facebookFirstName = response.first_name;
        facebookLastName = response.last_name;
        facebookEmail = response.email;

        $('#firstName').val(facebookFirstName);
        $('#lastName').val(facebookLastName);
        $('#email1').val(facebookEmail);

        $('#to-form').click();
    });
}

// Google Render
function googleRenderLogin() {
    //$('#social-loading').removeClass('d-none');
}

// Google Sign-In
function googleOnSignIn() {
    validateGoogleLoggedIn();
}

// Google Sign-Out
function signOutGoogle() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
}

// Validates if User is Logged in with Google
function validateGoogleLoggedIn() {
    var auth2 = gapi.auth2.getAuthInstance();
    if (auth2.isSignedIn.get()) {
        $('#social-loading').addClass('d-none');
        var profile = auth2.currentUser.get().getBasicProfile();

        googleId = profile.getId();
        googleEmail = profile.getEmail();
        googlePhoto = profile.getImageUrl();
        googleFirstName = profile.getGivenName();
        googleLastName = profile.getFamilyName();

        $('#email1').val(googleEmail);
        $('#firstName').val(googleFirstName);
        $('#lastName').val(googleLastName);

        $('#to-form').click();

        // $("#openingPage").addClass("d-none");
        // $("#connexionPage").addClass("d-none");
        // $("#step-container").removeClass("d-none");
        // $(".sidebar .wizard").removeClass("d-none");
        // $(".loanAmount").removeClass("d-none");
    }
}
// Validate UI
function validateUI(step) {
    var form = $("#myForm");
    var stepIsValid = 0;

    // Input text
    $(step + " input[type=text]").each(function () {
        var input = $(this);

        // Apt is not required
        // Extension not required
        if (input[0].id == 'appartment' || input[0].id == 'job_phone_ext') {
            console.warn("input: " + input.val() + " - stepIsValid: " + stepIsValid);
        } else {
            if (input.val() == "") {
                stepIsValid++ //= false;
                //form.addClass('was-validated');
            }
            console.log("input: " + input.val() + " - stepIsValid: " + stepIsValid);
        }

        // Minimum length Cel Phone
        if (input[0].id == 'celphone') {

            if ($('#celphone').unmask().val().length !== 10) {
                stepIsValid++
            }
        }

        // Minimum length Postal Code
        if (input[0].id == 'postalCode') {

            if ($('#postalCode').unmask().val().length !== 6) {
                stepIsValid++
            }
        }

        // Minimum length Ref1 Phone
        if (input[0].id == 'ref1_phone') {

            if ($('#ref1_phone').unmask().val().length !== 10) {
                stepIsValid++
            }
        }

        // Minimum length Ref2 Phone
        if (input[0].id == 'ref2_phone') {

            if ($('#ref2_phone').unmask().val().length !== 10) {
                stepIsValid++
            }
        }

        // Minimum length Job Phone
        if (step === "#EMPLOYEE") {
            if (input[0].id == 'job_phone') {

                if ($('#job_phone').unmask().val().length !== 10) {
                    stepIsValid++
                }
            }
        }

        // Minimum length Self-employed Phone
        if (step === "#SELF_EMPLOYED") {
            if (input[0].id == 'selfEmployed-phone') {

                if ($('#selfEmployed-phone').unmask().val().length !== 10) {
                    stepIsValid++
                }
            }
        }

        // if (input[0].id !== 'appartment' || input[0].id !== 'job_phone_ext') {
        //     if (input.val() == "") {
        //         stepIsValid++ //= false;
        //         //form.addClass('was-validated');
        //         console.log("input: " + input.val() + " - stepIsValid: " + stepIsValid);
        //     } else {
        //         //if (stepIsValid > 0) {
        //         //    stepIsValid--;
        //         //}
        //         //stepIsValid = true;
        //         //form.removeClass('was-validated');
        //         console.warn("input: " + input.val() + " - stepIsValid: " + stepIsValid);
        //     }
        // }
    });

    // Email
    $(step + " input[type=email]").each(function () {
        var input = $(this);
        var emailValid = true;

        if (input.val() == "") {
            stepIsValid++ //= false;
            //form.addClass('was-validated');
            console.log("input: " + input.val() + " - stepIsValid: " + stepIsValid);
        } else {

            // Validate email format
            if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(input.val())) {
                emailValid = false;
                stepIsValid++;
            }

            //if (stepIsValid > 0 && emailValid) {
            //    stepIsValid--;
            //}

            //stepIsValid = true;
            //form.removeClass('was-validated');
            console.warn("input: " + input.val() + " - stepIsValid: " + stepIsValid);
        }
    });

    // Dropdown
    $(step + " select").each(function () {
        var input = $(this);
        if (input.val() == "") {
            stepIsValid++ //= false;
            //form.addClass('was-validated');
            console.log("input: " + input.val() + " - stepIsValid: " + stepIsValid);
        } else {
            //if (stepIsValid > 0) {
            //    stepIsValid--;
            //}
            //stepIsValid = true;
            //form.removeClass('was-validated');
            console.warn("input: " + input.val() + " - stepIsValid: " + stepIsValid);
        }
    });

    return stepIsValid;
}

function nextTab(elem, isQuebec) {

    if (isQuebec) {
        $(elem).nextAll().eq(1).find('a[data-toggle="tab"]').click();
    } else {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }

    $('.firstName').html($('#firstName').val());
    $('.address').html($('#address').val() + '&nbsp;' + $('#street').val());
}

function prevTab(elem) {
    // If Province is not QC then hide the Obligations section
    var hash = window.location.hash.substr(1);
    var province = $('#province').val();

    if (province !== 'QC' && hash == 'step4') {
        $(elem).prevAll().eq(1).find('a[data-toggle="tab"]').click();
    } else {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }
}

$('.nav-tabs').on('click', 'li', function (e) {
    $('.nav-tabs li.active').removeClass('active').addClass('complete');
    $(this).removeClass('disabled').addClass('active');

    //$('.nav-tabs li.active').removeClass('active');
    //$(this).addClass('active');
});

$('.incomeSource .btn-group-toggle input').on('change', function () {
    $(".employee-section").addClass("d-none");
    $(".EA-section").addClass("d-none");
    $('.selfEmployed-section').addClass("d-none");
    $('.disabilityBenefits-section').addClass("d-none");
    $('.parentalInsurancePlan-section').addClass("d-none");
    var selected_incomeSource = $('input[name=incomeSourceOption]:checked', '#myForm').val()
    switch (selected_incomeSource) {
        case 'EMPLOYEE':
            $(".employee-section").removeClass("d-none");
            break;
        case 'EMPLOYMENT_INSURANCE':
            $(".EA-section").removeClass("d-none");
            break;
        case 'SELF_EMPLOYED':
            $(".selfEmployed-section").removeClass("d-none");
            break;
        case 'DISABILITY':
            $(".disabilityBenefits-section").removeClass("d-none");
            break;
        case 'PARENTAL_INSURANCE':
            $(".parentalInsurancePlan-section").removeClass("d-none");
            break;
        default:
    }
});

$('footer').on('click', '#to-connexion', function () {
    $('#openingPage').addClass("d-none");
    $("#connexionPage").toggleClass("d-none");
    ($('#step-container').is('.d-none')) ? '' : $("#step-container").addClass("d-none");
    ($('.sidebar .wizard').is('.d-none')) ? '' : $(".sidebar .wizard").addClass("d-none");
    ($('.loanAmount').is('.d-none')) ? '' : $(".loanAmount").addClass("d-none");
});
$('footer').on('click', '#to-firstPage', function () {
    $('#openingPage').removeClass("d-none");
    $("#connexionPage").addClass("d-none");
});


$('footer').on('click', '#to-form', function (e) {
    var form = $("#myForm");
    var auth2 = gapi.auth2.getAuthInstance();

    if ($('#email').val() === "" && (auth2.isSignedIn.get() || facebookLoggedIn)) {

        //$('#email2-div').show();

        $('#email1-div').hide();

        $("#openingPage").addClass("d-none");
        $("#connexionPage").addClass("d-none");
        $("#step-container").removeClass("d-none");
        $(".sidebar .wizard").removeClass("d-none");
        $(".loanAmount").removeClass("d-none");

    } else if ($('#email').val() !== "" && (!auth2.isSignedIn.get() || !facebookLoggedIn)) {

        // Validate email format
        if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($('#email').val())) {

        } else {
            $('#email1-div').show();
            //$('#email2-div').hide();

            $('#email1').val($('#email').val());

            $('#connexionPage').addClass("d-none");
            $("#step-container").removeClass("d-none");
            $(".sidebar .wizard").removeClass("d-none");
            $(".loanAmount").removeClass("d-none");
        }
    }
});


function start() {

}